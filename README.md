# General

```
C is general-purpose, procedural, imperative computer programming language!
```

1.General purpose

- https://www.rung.vn/dict/en_vn/General-purpose
- Nôm na chính là đa dụng

2.Procedural

```
Phương pháp thủ tục chia một chương trình (chức năng) lớn thành các khối chức năng hay hàm (thủ tục) đủ nhỏ để dễ lập trình và kiểm tra.
Mỗi hàm có một điểm bắt đầu và một điểm kết thúc và có dữ liệu và logic riêng.
```

- https://vi.wikipedia.org/wiki/L%E1%BA%ADp_tr%C3%ACnh_th%E1%BB%A7_t%E1%BB%A5c
- https://www.codehub.vn/Functional-Programming-va-Procedural-Programming-Khac-Nhau-Nhu-The-Nao

3.Imperative

```
In computer science, imperative programming is a programming paradigm that uses statements that change a program's state. In much the same way that the imperative mood in natural languages expresses commands, an imperative program consists of commands for the computer to perform. Imperative programming focuses on describing how a program operates.

The term is often used in contrast to declarative programming, which focuses on what the program should accomplish without specifying how the program should achieve the result.
```

- https://en.wikipedia.org/wiki/Imperative_programming
- https://viblo.asia/p/tim-hieu-ve-imperative-va-declarative-programming-nwmGyQLDGoW
- https://www.netguru.co/blog/imperative-vs-declarative
- https://tech.3si.vn/2015/09/19/declarative-programming/

## Dev tools

### Code Editor

- CodeBlocks
- Sublime Text
- CLion
- Netbean
- Eclipse
- Visual Studio

#### Sublime Text 3 Run C file with 1 step

- https://stackoverflow.com/questions/24225343/how-to-compile-and-run-c-in-sublime-text-3

**Tools > Build System > New Build System...**

Save with name: **C.sublime-build**

```json
{
    "cmd" : ["gcc", "$file_name", "-o", "${file_base_name}.exe"],

    // Doesn't work, sublime text 3, Windows 8.1    
    // "cmd" : ["gcc $file_name -o ${file_base_name}"],

    "selector" : "source.c",
    "shell": true,
    "working_dir" : "$file_path",

    // You could add path to your gcc compiler this and don't add path to your "PATH environment variable"
    // "path" : "C:\\MinGW\\bin"

    "variants" : [

        { "name": "Run",
          "cmd" : ["${file_base_name}.exe"]
        }
    ]
}
```

Ctrl + Shift + B:

- C : build
- C-Run: run

### Build

Windows:

- Install MingW or Cygwin
- Install common packages used for build : gcc, ...

# Getting start

- [Compile C Online](https://www.tutorialspoint.com/compile_c_online.php)
- https://www.geeksforgeeks.org/executing-main-in-c-behind-the-scene/
- https://stackoverflow.com/questions/7494244/how-to-change-entry-point-of-c-program-with-gcc
- https://www.techiedelight.com/c-program-without-main-function/
- https://www.geeksforgeeks.org/executing-main-in-c-behind-the-scene/

## The standard lib

- https://www.tutorialspoint.com/c_standard_library/limits_h.htm

## 1.A C program basically consists of the following parts

- Preprocessor Commands
- Functions
- Variables
- Statements & Expressions
- Comments

```bash
PS E:\code\public\c-lang\lession001> gcc -o sample.exe main.c
PS E:\code\public\c-lang\lession001> objdump -f .\sample.exe

.\sample.exe:     file format pei-x86-64
architecture: i386:x86-64, flags 0x0000013a:
EXEC_P, HAS_DEBUG, HAS_SYMS, HAS_LOCALS, D_PAGED
start address 0x00000000004014e0

PS E:\code\public\c-lang\lession001>
```

Take a look at this

```c
#include <stdio.h>

int main() {
   /* my first program in C */
   printf("Hello, World! \n");
   
   return 0;
}
```

"#include <stdio.h>": is a preprocessor which tells a C compiler to include stdio.h file before going to actual compilation


"int main()": is the main function when there program execution begins

Let's try to change this function name, what's happened?

- Errors when build, because the compiler

Explain:

```
The executable file created after compiling a C source code is a Executable and Linkable Format (ELF) file.
Every ELF file have a ELF header where there is
 a e_entry field which contains the program memory address from which the execution of executable will start. This memory address point to the _start() function.
After loading the program, loader looks for the e_entry field from the 
ELF file header. Executable and Linkable Format (ELF) is a common standard file format used in UNIX system for executable files, object code, shared libraries, and core dumps.
```

Tip :Override _start function



```c
int _start()
{
    int r = begin(); // calling custom main function

    exit(r);
}

int begin()
{
    printf("Begin function");
    return 0;
}

```

```bash
gcc -nostartfiles -o sample.exe .\main.c
```