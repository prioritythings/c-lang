#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 255

int main(int argc, char const *argv[])
{

	// https://www.hackerrank.com/challenges/playing-with-characters/problem
	// https://en.cppreference.com/w/c/io/fscanf
	// https://www.programiz.com/c-programming/c-input-output
	char c, s[30], sen[255];	
	scanf("%c %s %[^\n]", &c, &s, &sen);
	printf("%c\n%s\n%s\n", c, s, sen);
	printf("%s\n", "End!");
	return 0;
}