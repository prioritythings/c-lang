#include <stdio.h>

void writeBinData(char *s);
void readBinData(char *);

int main(int argc, char const *argv[])
{
	// https://www.programiz.com/c-programming/c-file-input-output
	/* code */
	FILE *fp;

	// open
	fp = fopen("tmp/sample.txt", "a+");
	fputc( 'C', fp);
	fprintf(fp, "%s\n", "C is a general-purpose, procedural, imperative programming language");
	// close
	fclose(fp);
	writeBinData("some thing");
	readBinData("something");
	return 0;
}

void writeBinData(char *s){
	char str[50] = "a private password: 12345678";
	FILE *fp;
	fp = fopen("tmp/sample.bin", "ab+");
	fwrite(str, sizeof(str), 1, fp);
	fclose(fp);
}

void readBinData(char *s){
	FILE *fp;
	char str[50];
	fp = fopen("tmp/sample.bin", "ab+");
	fread(str, sizeof(str), 1, fp);
	fclose(fp);
	printf("%s\n", str);
}