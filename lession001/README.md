# Executing Main in C Behind the scene

- https://www.geeksforgeeks.org/executing-main-in-c-behind-the-scene/
- https://www.sanfoundry.com/objdump-command-usage-examples-in-linux/
- https://www.thegeekstuff.com/2012/09/objdump-examples/
- https://www.thegeekstuff.com/2010/12/50-unix-linux-sysadmin-tutorials/
- https://www.thegeekstuff.com/2010/11/50-linux-commands/

**Build**

```bash
gcc -o sample.exe main.c
```

**Check the address**

```bash
objdump -f sample.exe
```

output

```bash
PS E:\code\public\c-lang\lession001> objdump -f sample.exe

sample.exe:     file format pei-x86-64
architecture: i386:x86-64, flags 0x0000013a:
EXEC_P, HAS_DEBUG, HAS_SYMS, HAS_LOCALS, D_PAGED
start address 0x00000000004014e0
```

# The difference between a declaration and a definition

- Debate: https://stackoverflow.com/questions/1410563/what-is-the-difference-between-a-definition-and-a-declaration


- The term **declaration** means (in C) that you are telling the compiler about type, size and in case of function declaration, type and size of its parameters of any variable, or user defined type or function in your program. **No space is reserved** in memory for any variable in case of declaration. However compiler knows how much space to reserve in case a variable of this type is created.

- **Definition** on the other hand means that in additions to all the things that declaration does, **space is also reserved in memory**. You can say "DEFINITION = DECLARATION + SPACE RESERVATION" following are examples of definition:


```c
// except one all these are definitions
int a;                                  // defines a
extern const int c = 1;                 // defines c
int f(int x) { return x + a; }          // defines f and defines x
struct S { int a; int b; };             // defines S, S::a, and S::b
struct X {                              // defines X
    int x;                              // defines non-static data member x
    static int y;                       // DECLARES static data member y
    X(): x(0) { }                       // defines a constructor of X
};
int X::y = 1;                           // defines X::y
enum { up , down };                     // defines up and down
namespace N { int d; }                  // defines N and N::d
namespace N1 = N;                       // defines N1
X anX;                                  // defines anX


// all these are declarations
extern int a;                           // declares a
extern const int c;                     // declares c
int f(int);                             // declares f
struct S;                               // declares S
typedef int Int;                        // declares Int
extern X anotherX;                      // declares anotherX
using N::d;                             // declares N::d


// specific to C++11 - these are not from the standard
enum X : int;                           // declares X with int as the underlying type
using IntVector = std::vector<int>;     // declares IntVector as an alias to std::vector<int>
static_assert(X::y == 1, "Oops!");      // declares a static_assert which can render the program ill-formed or have no effect like an empty declaration, depending on the result of expr
template <class T> class C;             // declares template class C
;                                       // declares nothing
```

# Static const vs define

- https://www.baldengineer.com/const-vs-define-when-do-you-them-and-why.html
- https://stackoverflow.com/questions/1674032/static-const-vs-define-vs-enum


# C storage classes

- https://www.geeksforgeeks.org/storage-classes-in-c/

- **auto**: This is the default storage class for all the variables declared inside a function or a bloc
- **extern**:  The main purpose of using extern variables is that they can be accessed between two different files which are part of a large program

- **static**: we can say that they are initialized only once and exist till the termination of the program. Thus, no new memory is allocated because they are not re-declared. Their scope is local to the function to which they were defined. Global static variables can be accessed anywhere in the program. By default, they are assigned the value 0 by the compiler.

- **register**: An important and interesting point to be noted here is that we cannot obtain the address of a register variable using pointers

```bash
PS E:\code\public\c-lang\lession001> gcc -o sample.exe .\main.c .\utils.c
PS E:\code\public\c-lang\lession001> .\sample.exe
Count is 5
PS E:\code\public\c-lang\lession001> gcc .\main.c .\utils.c
PS E:\code\public\c-lang\lession001> .\a.exe
Count is 5
PS E:\code\public\c-lang\lession001>
```

# Initialize

```
It is a good programming practice to initialize variables properly, otherwise your program may produce unexpected results, because uninitialized variables will take some garbage value already available at their memory location.
```

# Array

```
Arrays a kind of data structure that can store a fixed-size sequential collection of elements of the same type
```

- Multi dimensional array
- Passing array to function : https://codeforwin.org/2017/12/pass-return-array-function-c.html
- Dynamic array : http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/2-C-adv-data/dyn-array.html

# Common issues

- https://www.geeksforgeeks.org/rand-and-srand-in-ccpp/
- https://www.geeksforgeeks.org/pointers-vs-references-cpp/
- https://cboard.cprogramming.com/c-programming/160928-expanding-array-malloc.html
- http://www.cplusplus.com/reference/cstdlib/realloc/

# Giải quyết các bài toán sau:

1. Liệt kê các kiểu dữ liệu trong C với thông tin đi kèm: type, storage size, value range(min to max), precision( độ chính xác )
2. Lưu thông tin ở 1 vào 1 file có tên là c-data-types.txt