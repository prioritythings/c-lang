#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

extern int i, j;
extern void print_something();
const int PIN = 13;
static int count = 10;
int *sortArray(int arr[], int size);
int *randomArray(int size);
int sumArray(int *arr, int size);
void getCurrentTime(unsigned long *sec);
unsigned long mystrlen(char *);
void mystrcpy(char s1[], char s2[]);
void mySort(int arr[], int size);
// #define PIN 13

// https://www.tutorialspoint.com/c_standard_library/limits_h.htm

int main()
{
  printf("C is a general-purpose, procedural, imperative computer programming language developed in \
1972 by Dennis M. Ritchie at the Bell Telephone Laboratories to develop the UNIX operating system.!\n");
  // Integer types
/*  printf("Type:%s\n", "char");
  printf("Storage size:%d\n", sizeof(char));
  printf("Value range: (%d - %d)\n", CHAR_MIN, CHAR_MAX);

  printf("Type:%s\n", "unsigned char");
  printf("Storage size:%d\n", sizeof(unsigned char));
  printf("Value range: (%d - %d)\n", 0, UCHAR_MAX);

  printf("Type:%s\n", "signed char");
  printf("Storage size:%d\n", sizeof(signed char));
  printf("Value range: (%d - %d)\n", SCHAR_MIN, SCHAR_MAX);

  printf("Type:%s\n", "int");
  printf("Storage size:%d\n", sizeof(int));
  printf("Value range: (%d - %d)\n", INT_MIN, INT_MAX);

  int i;
  int j;
  j = 10;

  printf("Declaration:i=%d, j=%d\n", i, j);

  printf("PIN:%d\n", PIN);
  counter();
  count = 5;
  counter();
  while(count > 0){
    static int y_count = 5;
    y_count++;
    printf("count = %d, count_y = %d\n", count, y_count);
    count--;
  }*/
  // print_something();

  // infinite loop
  // for( ; ; ) {
  //   printf("This loop will run forever.\n");
  // }

/*  int x1 = 1, y1 = 2;
  printf("Before swap: x1=%d, y1=%d\n", x1 , y1);
  swap(&x1, &y1);
  printf("After swap: x1=%d, y1=%d\n", x1 , y1);
  printf("Max of %d,%d = %d", x1, y1, max(x1, y1));

  int fiboArr[5];// int = 4bytes ==> 5 int ==> 20 bytes

  printf("fiboArr size:%d\n", sizeof(fiboArr) / sizeof(int));
  for(i = 0; i < 5; i++){
    printf("fiboArr[%d]: %d\n", i, fiboArr[i]); // garbage value
  }

  for(i = 0; i < 5; i++){
    fiboArr[i] = i * 2;
    printf("fiboArr[%d]: %d\n",i , fiboArr[i]);
  }

  int dayOfWeeks[7] = { 2,3,4,5,6,7,8 };
  for(i = 0; i < 7; i++){
    printf("dayOfWeeks[%d]: %d\n", i, dayOfWeeks[i]);
  }
  printf("Max day of Weeks: %d\n", maxElement(&dayOfWeeks, sizeof(dayOfWeeks)/sizeof(int)));

  int matrix[2][2] = {
    {1,2},
    {3,4}
  };

  for(i=0; i < 2; i++){
    for(j=0; j < 2; j++){
      printf("%d", matrix[i][j]);
    }
    printf("\n");
  }

  int unsortArr[8] = {1,9,1,2,1,4,1,3};
  int randomArr[100];
  printf("Size if randomArr: %d\n", sizeof(randomArr) / sizeof(int));
  
  printf("Size if unsortArr: %d\n", sizeof(unsortArr) / sizeof(int));
  printArr(unsortArr, 8);

  int *newArr[8];  
  printArr(newArr, 8);

  int * sArr;
  sArr = sortArray(unsortArr, 8);
  printArr(sArr, 8);*/

  // int *randomArr001;
  // randomArr001 = randomArray(10);
  // printArr(randomArr001, 10);

/*  int *randomArr001;
  int i,size = 10;  
  randomArr001 = sortArray( randomArray(size), size );
  
  // randomArr001 = calloc(size, sizeof(int));

  for(i = 0 ; i < size; i++){
    // *(randomArr001+i) = rand();
    printf("%d\n",randomArr001[i]);
  }
  printf("SUM=%d", sumArray(randomArr001, size));

  free(randomArr001);

  randomArr001
*/
/*  int number = 10;
  printf("Address of number variable: %x\n", &number );  
  int* p = &number; // pointer is a derived type, it store the address of object
  printf("Address of number variable: %x\n", p );
  printf("%d\n", *p);
  printf("%d\n", *(&number));

  int numbers[] = {11, 22, 33, 44, 55};

  int* pnumbers = numbers; // array is derived data type

  printf("Address:%x\n", numbers);
  printf("Address:%x\n", pnumbers);
  int i;
  // duyet mang thuan
  for(i=0;i<sizeof(numbers)/sizeof(int);i++){
    // printf("Address at %d: %x\n", i, pnumbers + i);
    printf("Address at %d: %x\n", i, numbers + i);
    printf("Current Value of pointer:%d\n", *pnumbers);
    printf("Current Address of pointer:%x\n", pnumbers++);// print before increase    
  }
  // duyet mang nguoc
  pnumbers = &numbers[sizeof(numbers)/sizeof(int) - 1]; // get address of last element in array

  for(i=0;i<sizeof(numbers)/sizeof(int);i++){
    printf("Current Value of pointer:%d\n", *pnumbers);
    printf("Current Address of pointer:%x\n", pnumbers--);// print before decrease
  }

  // array of pointers
  printf("Array of pointers\n");
  int *p_arr[5];
  for(i=0;i<5;i++){
    p_arr[i] = &numbers[i];
    printf("%d\n", *p_arr[i]);
  }

  // strings are actually one-dimension array of characters
  char str[6] = "abcdef";
  printf("%c\n", str[0]);
  printf("%s\n", str);

  // dynamic strings
  char *pstr = "abcdefgh";
  printf("%x\n", &pstr);

  char *strings[] = {
    "the first line",
    "the second line",
    "the third line"
  };

  char **ps;

  for(i = 0; i < 3; i++){
    printf("%s\n", strings[i]);
    printf("%x\n", &strings[i]);
    ps = &strings[i];
    printf("%x\n", ps);
    printf("%s\n", *ps);
  }

  // Passing pointer to function
  int x = 5;
  int *xp = &x;
  *xp = 8;
  printf("%d\n", *xp);

  unsigned long sec;
  getCurrentTime(&sec);

  printf("%d\n", sec);*/

  // sizeof

/*   int *pi;
   double *pd;
   long *pl;
   char *pc;
   int i;
   int j = 4;
   char c;
   
   

   printf("Size of Int Pointer: %d\n", sizeof(pi));
   printf("Size of Double Pointer: %d\n", sizeof(pd));
   printf("Size of Long Pointer: %d\n", sizeof(pl));
   printf("Size of Char Pointer: %d\n", sizeof(pc));
   printf("Size of Int: %d\n", sizeof(i));
   printf("Size of Int: %d\n", sizeof(j));
   printf("Size of Char: %d\n", sizeof(c));*/  

  // array
/*  int nums[] = {1,4,3,2,1};
  mySort(nums, 5);
  printArr(nums, 5);*/

  // string master
  char *str1 = "The API";
  char *str2 = "values";
  char strn[6] = "abcxyz";

  int nums[] = {1,2,3};

  printf("%c\n", *str1);
  printf("%c\n", *(str1 + 1));
  printf("%c\n", *(str1 + 2));
  printf("%c\n", *(str1 + 7));
  printf("%d\n", sizeof(strn)/sizeof(strn[0]));
  printf("Length of %s: %d\n", "strn", sizeof(strn)/sizeof(strn[0])); // string define with pointers always end with '\0' : null-terminated string
  printf("%d\n", sizeof(nums)/sizeof(nums[0]));

  // strcpy
  // char *str3 = str1;
  // strcpy(str3, str2);
  char *str3;
  // str3 = calloc(10, sizeof(char));
  str3 = malloc(10 * sizeof(char));
  strcpy(str3, str2);
  printf("%s\n", str3);
  free(str3);

  // strcat
  str3 = str1;
  int i;
  int len3 = mystrlen(str3);
  
  printf("Str3Len:%d\n", mystrlen(str3)); // pointers in 64 system has 8 bytes, in 32 has 4 bytes
  printf("Str3:%s\n", str3);

  char s1[3] = "abc";
  char s2[3] = "xyz";
  char *s3;
  s3 = malloc(sizeof(char) * 6);
  for(i = 0; i< mystrlen(s1); i++){
    s3[i] = s1[i];
  }
  for(i = 0; i<mystrlen(s2); i++){
    printf("i:%i\n", mystrlen(s1)+i);
    s3[mystrlen(s1)+i] = s2[i];
  }
  char *p = s1;
  p = s3;
  // p = realloc(p, sizeof(char)*mystrlen(s3));
  // strcat(s1,s2);
  printf("s1:%s\n",p);
  printf("s3:%s\n",s3);

  return 0;
}

unsigned long mystrlen(char *str){
  char *p = str;
  unsigned long n = 0;
  while(*p){
    // printf("[%d]=%c\n", n, *p);
    p++;
    n++;
  }  
  return n;
}

void mySort(int arr[], int size)
{
  printf("%x\n", &arr);
  int i,j , tmp;
  for(i = 0; i<size;i++){    
    for(j=i+1;j<size;j++){
      if(arr[j] < arr[i]){
        tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
      }
    }
  }
}


printArr(int arr[100], int size){
  // Those data types which are derived from the fundamental data types are called derived data types. Function, arrays, and pointers are derived data types in C programming language.
  int i;  
  printf("Size: %d\n", sizeof(arr) / sizeof(int));// always eq 2
  for(i = 0; i < size; i++){
    printf("%d ", arr[i]);
  }
  printf("\n");
  return;
}

int counter(void){
  printf("Counter: %d\n", count);
  return 0;
}

// swap(int x, int y){
//   int tmp;

//   tmp = x;
//   x = y;
//   y = tmp;

//   return;
// }

swap(int *x, int *y){
  int tmp;
  tmp = *x;
  *x = *y;
  *y = tmp;
  return ;
}


int max(x, y){
  if(x > y){
    return x;
  }
  return y;
}

int maxElement(int *arr, int length){
  int max = arr[0];
  int i;
  for(i = 1; i < length; i++){
    if(arr[i] > max){
      max = arr[i];
    }
  }
  return max;
}

int * sortArray(int arr[], int size)
{
  int i,j;
  printf("Array inside function: \n");
  // Print value of each array element
  for (i = 0; i < size - 1; ++i)
  {
    for(j = i + 1; j < size; j++){
      if(arr[j] < arr[i]){
        swap(&arr[j], &arr[i]);
      }
    }
  }
  return arr;
}


int sumArray(int arr[], int size)
{
  printf("Size:%d\n", size);
  int sum = 0;
  while(size > 0){
    sum += arr[--size];
  }
  return sum;
}

int *randomArray(int size)
{
  // Define a reference variable (say p) of the desired data type
  int* arr;
  
  // cap phat bo nho
  arr = calloc(size, sizeof(int));
  int i;
  // use current time as seed for random generator
  srand(time(0));
  for(i = 0; i<size; i++){
    *(arr+i) = rand() % 51; // max is 50
  }
  
  return arr;
}

void getCurrentTime(unsigned long *sec){
  *sec = time(NULL);
  return;
}