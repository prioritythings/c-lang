#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <stdlib.h>

#define LEFT_ARROW 75
#define RIGHT_ARROW 77
#define UP_ARROW 72
#define DOWN_ARROW 80

void draw(int position, int n);
void calculate_pos(int c, int *i, int *j, int n);

int main(int argc, char const *argv[])
{
	// char
	int c;
	// printf("%s\n", "Please press enter to start the game");
	
	// while(c != 10){//enter
	// 	c = getchar();
	// }

	// printf("ASCII value: %d\n", c);
	// putchar(c);
	int i=1 , j=1, n=10, position = i*10 + j;
	while(c != 27){//esc
		draw(position, n);
		c = getch();
		calculate_pos(c, &i, &j, n);
		position = i * 10 + j;
	}

	return 0;
}

void calculate_pos(int c, int *i, int *j, int n)
{
	if(c==LEFT_ARROW){
		*j = *j - 1;
		if(*j == 0){
			*j = n;
		}
	}
	if(c==RIGHT_ARROW){
		*j = *j + 1;
		if(*j == n + 1){
			*j = 1;
		}
	}
	if(c==UP_ARROW){
		*i = *i - 1;
		if(*i== 0){
			*i = n;
		}
	}
	if(c==DOWN_ARROW){
		*i = *i + 1;
		if(*i == n + 1){
			*i = 1;
		}
	}	
}

void draw(int position, int n)
{
	system("cls");
	int i,j;
	char c = ' ';

	for(i = 1;i <= n; i++){
		for(j = 1; j <= n; j++){
				c = (position == i*10 + j) ? '*' : ' ';
				putchar(c);
		}
		printf("\n");
	}
}