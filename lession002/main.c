#include <stdio.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

struct Address
{
	char name[50];
	char street[100];
	char city[50];
	char state[20];
	char status;// E:enabled, D:disabled
	char isValid : 1; // 1 bit: 0 | 1
	unsigned long pin;
};

union Job
{
   float salary;
   int workerNo;
} j;

typedef struct Careers{
	char name[50];
	unsigned int salary;
} Career;

void printAddress( struct Address* address );

int main(int argc, char const *argv[])
{
	// https://stackoverflow.com/questions/3683602/single-quotes-vs-double-quotes-in-c-or-c
	char c1 = 'x';// this is character
	char *s1 = "x"; // this is string
	printf("c1=%c\n", c1);
	printf("s1=%s\n", s1);
	// struct : https://www.geeksforgeeks.org/structures-c/

	struct Address homeAddress;
	strcpy(homeAddress.name, "Home");
	strcpy(homeAddress.street, "116 Tran Quoc Toan, P7, Q3");
	strcpy(homeAddress.city, "TP.Ho Chi Minh");
	strcpy(homeAddress.state, "");
	homeAddress.status = 'E';
	homeAddress.pin = 76000;
	homeAddress.isValid = FALSE;

	// printf("%s\n", homeAddress.name);
	// printf("%s\n", homeAddress.street);
	// printf("%s\n", homeAddress.city);
	// printf("%d\n", homeAddress.pin);
	printf("%d\n", sizeof(homeAddress));
	printAddress(&homeAddress);

	// Why do we need c unions: https://stackoverflow.com/questions/252552/why-do-we-need-c-unions
	j.salary = 12.3;
	j.workerNo = 100;

	printf("Salary = %.1f\n", j.salary);
	printf("Number of workers = %d\n", j.workerNo);

	Career c;
	strcpy(c.name, "Ban hang");
	c.salary = 200;
	printf("Career: %s\n", c.name);
	printf("Salary: %d USD\n", c.salary);	
	return 0;
}


void printAddress( struct Address* address ){
	printf("%s\n", address->name);
	printf("%s\n", address->street);
	printf("%s\n", address->city);
	printf("%d\n", address->pin);
	printf("%s\n", address->isValid ? "Valid" : "Not valid");
}