#include <stdio.h>
#include <string.h>
// Alignment requirements 
// (typical 32 bit machine) 
  
// char         1 byte 
// short int    2 bytes 
// int          4 bytes 
// double       8 bytes 
  
// structure A 
typedef struct structa_tag 
{ 
   char        c; // 1 byte
   short int   s; // 2 byte
} structa_t; // sizeof(char) + 1(padding) + sizeof(short) = 1 + 1 + 2 = 4 bytes
  
// structure B 
typedef struct structb_tag 
{ 
   short int   s; 
   char        c; 
   int         i; 
} structb_t; // sizeof(short) + sizeof(char) + 1(padding) + sizeof(int) = 8 bytes
  
// structure C 
typedef struct structc_tag 
{ 
   char        c; //1 byte
   double      d; // 8 bytes
   int         s; // 4 bytes
} structc_t;  // 1 + 8 + 4 ==> 1 + 7 + 8 + 4 + 4 = 24
  
// structure D 
typedef struct structd_tag 
{ 
   double      d; 
   int         s; 
   char        c; 
} structd_t;// 8 + 4 + 1 ==> 8 + 4 + 1 + 3 = 16

int main() 
{ 
   printf("%s\n", "Inorder to avoid such misalignment, compiler will introduce alignment requirement to every structure. It will be as that of the largest member of the structure");

   // https://www.geeksforgeeks.org/structure-member-alignment-padding-and-data-packing/
   printf("sizeof(structa_t) = %d\n", sizeof(structa_t)); 
   printf("sizeof(structb_t) = %d\n", sizeof(structb_t)); 
   printf("sizeof(structc_t) = %d\n", sizeof(structc_t)); 
   printf("sizeof(structd_t) = %d\n", sizeof(structd_t)); 
  
   return 0; 
}