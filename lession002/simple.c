#include <stdio.h>
#include <string.h>
#include <time.h>

typedef struct Persons
{
	char name[50];
	char dob[10];// date of birth.Format:dd-mm-yyyy
	unsigned short int age;
} Person;

int main(int argc, char const *argv[])
{
	// http://www.cplusplus.com/reference/cstdio/scanf/
	// http://www.gnu.org/software/libc/manual/html_node/String-Input-Conversions.html
	// https://regex101.com/
	// https://www.quora.com/What-is-meaning-of-*-n-*c-while-using-scanf-in-c
	// UTC time : http://zetcode.com/articles/cdatetime/
	Person p;

	time_t now = time(NULL); // The time() function returns the value of time in seconds since 0 hours, 0 minutes, 0 seconds, January 1, 1970, Coordinated Universal Time
	if( now == -1){
		puts("The time() function failed!");
	}

	struct tm *ptm = localtime(&now);
  if (ptm == NULL) {     
    puts("The localtime() function failed");     
  }
    
  printf("The time is: %2d-%2d-%4d %02d:%02d:%02d\n",
  	ptm->tm_mday, ptm->tm_mon, ptm->tm_year + 1900,
  	ptm->tm_hour, ptm->tm_min, ptm->tm_sec);

	printf("Now : %ld\n", now);
	char c;
	puts("press something");
	while((c = getchar()) != 27){

	}

	puts("end!\n");
	
	// scanf("%[^\n]", &p.name);
	// scanf("%s", &p.name);
/*	printf("%s", "Please enter your name:");
	scanf("%[^\n]%*c", &p.name);
	printf("%s", "Please enter your date of birth with this format dd-mm-yyyy:");
	scanf("%10s", &p.dob);

	printf("Name:%s\n", p.name);
	printf("Date of birth:%s\n", p.dob);*/
	
	
	/* code */
	// char *name;
	// printf("Address of name: %x\n", &name);
	// printf("%s\n", "Can you enter your name?");
	// scanf("%s", &name);
	// printf("Hello %s\n", &name);

	return 0;
}